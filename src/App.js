import React,{Component} from 'react'; 
//import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
//import { useState, useEffect } from 'react';


class App extends Component { 

	constructor(props){
		super(props);
	
		this.state = {
		  values: {},
		  showValues: {}
		}
	  }
	  
   setSim(info ,text)
   {
	   let values = this.state.values;
	   values[info] = text;  
	   this.setState({values: values});
   }
	// On file select (from the pop up) 
	onFileChange = event => { 
	
	// Read file with FileReader 

  let file = event.target.files[0];
  let reader = new FileReader();
  var temp;
  reader.onload = function(event) {
	temp = event.target.result;
    //console.log(temp + "belul");
};
  reader.readAsText(file);
  reader.onloadend = () => {
	this.setSim("SimName", temp);
	this.setSim("SimType", temp);
	this.setSim("SimInfo", temp);
};
	};

	handleChange(name, e){
		let values = this.state.values;
	  values[name] = e.target.value;
		this.setState({values: values})
	}

	
	
	
	render() { 
	
	return ( 
		<div> 
			<h3> 
			Fill from File 
			</h3> 
			<div> 
				<input type="file" onChange={this.onFileChange} /> 
			</div> 
			<form noValidate autoComplete="off">
      			<TextField value = {this.state.values["SimName"]} onChange = {this.handleChange.bind(this,"SimName")} id="Simulation-Name" label="Simulation name" />
      			<TextField value = {this.state.values["SimType"]} onChange = {this.handleChange.bind(this,"SimType")} id="Simulation-type" label="Simulation type" variant="filled" />
      			<TextField value = {this.state.values["SimInfo"]} onChange = {this.handleChange.bind(this,"SimInfo")} id="Simulation-info" label="Simulation-info" variant="outlined" />
    		</form>
		</div> 
	); 
	} 
} 

export default App; 
